package com.akbar.thymeleaf.web;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.akbar.thymeleaf.model.Department;
import com.akbar.thymeleaf.repository.DepartmentRepository;


@Controller
@RequestMapping("/departments/")
public class DepartmentController {

	
	private final DepartmentRepository departmentRepository;

	@Autowired
	public DepartmentController(DepartmentRepository departmentRepository) {
		this.departmentRepository = departmentRepository;
	}

	@GetMapping("signup")
	public String showSignUpForm(Department department) {
		return "add-department";
	}

	@GetMapping("list")
	public String showUpdateForm(Model model) {
		model.addAttribute("departments", departmentRepository.findAll());
		return "index-department";
	}

	@PostMapping("add")
	public String addDepartment(@Valid Department department, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-department";
		}

		departmentRepository.save(department);
		return "redirect:list";
	}

	@GetMapping("edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Department department = departmentRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
		model.addAttribute("employee", department);
		return "update-department";
	}
	@PostMapping("update/{id}")
	public String updateDepartment(@PathVariable("id") long id, @Valid Department department, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			department.setId(id);
			return "update-department";
		}

		departmentRepository.save(department);
		model.addAttribute("departments", departmentRepository.findAll());
		return "index";
	}

	@GetMapping("delete/{id}")
	public String deleteDepartment(@PathVariable("id") long id, Model model) {
		Department department = departmentRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
		departmentRepository.delete(department);
		model.addAttribute("departments", departmentRepository.findAll());
		return "index";
	}
}
