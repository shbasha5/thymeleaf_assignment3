package com.akbar.thymeleaf.web;

//import java.util.List;
//import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;

//import com.akbar.thymeleaf.exception.RecordNotFoundException;
import com.akbar.thymeleaf.model.Employee;
import com.akbar.thymeleaf.repository.EmployeeRepository;
//import com.akbar.thymeleaf.service.EmployeeServices;

@Controller
@RequestMapping("/employees/")
public class EmployeeController {

	private final EmployeeRepository employeeRepository;

	@Autowired
	public EmployeeController(EmployeeRepository employeeRepository) {
		this.employeeRepository = employeeRepository;
	}

	@GetMapping("signup")
	public String showSignUpForm(Employee employee) {
		return "add-employee";
	}

	@GetMapping("list")
	public String showUpdateForm(Model model) {
		model.addAttribute("employees", employeeRepository.findAll());
		return "index";
	}

	@PostMapping("add")
	public String addEmployee(@Valid Employee employee, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-employee";
		}

		employeeRepository.save(employee);
		return "redirect:list";
	}

	@GetMapping("edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Employee employee = employeeRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
		model.addAttribute("employee", employee);
		return "update-employee";
	}
	@PostMapping("update/{id}")
	public String updateEmployee(@PathVariable("id") long id, @Valid Employee employee, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			employee.setId(id);
			return "update-student";
		}

		employeeRepository.save(employee);
		model.addAttribute("employees", employeeRepository.findAll());
		return "index";
	}

	@GetMapping("delete/{id}")
	public String deleteEmployee(@PathVariable("id") long id, Model model) {
		Employee employee = employeeRepository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Invalid student Id:" + id));
		employeeRepository.delete(employee);
		model.addAttribute("employees", employeeRepository.findAll());
		return "index";
	}
}
