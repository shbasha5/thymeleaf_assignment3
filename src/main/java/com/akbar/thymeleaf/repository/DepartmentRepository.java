package com.akbar.thymeleaf.repository;


import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.akbar.thymeleaf.model.Department;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {

    List<Department> findByName(String name);

}
